/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native'; 
import Pokemon from "./source/Components/Pokemon";
const styles = StyleSheet.create({
  
  container:{
      display:"flex",
      alignItems:"center"
  }
});
class App extends React.Component{
  render()
  {
    return(
      <View style={styles.container}>
        <Pokemon/>
      </View>
    )
  }
}
export default App;

