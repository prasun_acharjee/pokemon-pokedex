import firebase from "firebase";
const firebaseConfig = {
    apiKey: "AIzaSyC7cNJdVymx5tepKUszpUPWafJAvBBlr6g",
    authDomain: "pokemon-5d4ed.firebaseapp.com",
    databaseURL: "https://pokemon-5d4ed.firebaseio.com",
    projectId: "pokemon-5d4ed",
    storageBucket: "pokemon-5d4ed.appspot.com",
    messagingSenderId: "695872583728",
    appId: "1:695872583728:web:12f43218ee83fc2638d5f5",
    measurementId: "G-6GTJ9KHEZW"
  };
  firebase.initializeApp(firebaseConfig);
  export default firebase;