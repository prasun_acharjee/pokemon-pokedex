import React from "react";
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    Image
  } from 'react-native';
  const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        borderColor:'blue',
        borderWidth:1,
        margin:10,
        backgroundColor:'wheat'
    }
  });
class Pokemon extends React.Component{
    constructor(props)
    {
        super(props)
        this.state={pokeList:[],limit:20}
    }

    componentDidMount()
    {
        fetch(`https://pokeapi.co/api/v2/pokemon?limit=20`).then((res)=>res.json()).then((data)=>console.log(data));   
        fetch(`https://pokeapi.co/api/v2/pokemon?limit=20`).then((res)=>res.json()).then((data)=>{
            data.results.forEach((poke)=>{
               fetch(poke.url).then((res)=>res.json()).then((data)=>{
                   console.log(data);
                   this.setState({pokeList:this.state.pokeList.concat(data)})
               })
            })
        })
    }
    render()
    {
        return(
            <View>
               <FlatList 
                data={this.state.pokeList} 
                renderItem={({item})=> {
                   return(
                       <View style={styles.container}>
                        <Image source={{uri:item.sprites.front_default}} style={{height:300,width:300}}/>
                        <Text style={{fontSize:50,fontStyle:"Bold"}}>{item.species.name}</Text>
                       </View>
                   )
                }}
                showsVerticalScrollIndicator={false}
                onEndReachedThreshold={0.8} 
                onEndReached={({ distanceFromEnd }) => {
                this.setState({
                    limit:this.state.limit+20
                },()=>{
                    fetch(`https://pokeapi.co/api/v2/pokemon?offset=${this.state.limit}`).
                    then((res)=>
                    res.json())
                    .then((resp)=>{
                    resp.results.forEach((poke)=>{
                    fetch(poke.url)
                    .then((ress)=>ress.json())
                    .then((response)=>{
                    console.log(response);
                    this.setState({pokeList:this.state.pokeList.concat(response)})
                                })
                            })
                        })
                    })
                }}/>             
            </View>
        )
    }
}
export default Pokemon;